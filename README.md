# Decoupled Menu Parser

## Index

- Introduction
- Requirements
- Installation
- Usage
- Development
- Maintainers

## Introduction

A JavaScript library for parsing menus served by Drupal's menu linkset endpoint. See the [decoupled menus documentation](https://www.drupal.org/docs/develop/decoupled-drupal/decoupled-menus) for more information about this endpoint.

NOTE: Currently the linkset functionality from the decoupled menus contributed module is being ported to Drupal Core. Please check [this issue on Drupal.org](https://www.drupal.org/project/drupal/issues/3227824) to know more about the current status.

## Requirements

This project has no other node package as a peer dependency.

## Installation

Install the package via `npm`:

```sh
$ npm i @drupal/decoupled-menu-parser
```

## Usage

```js
import { denormalize, MenuInterface, NormalizedMenuInterface } from '@drupal/decoupled-menu-parser';
```

### denormalize

Denormalizes a set of links into an instance of a Menu.

| Params            | Type                       | Description                                    |
| ----------------- | -------------------------- | ---------------------------------------------- |
| normalized        | `NormalizedMenuInterface ` | The `parsed` menu data as recieved from drupal |
| menuID (optional) | `string`                   | The menu identifier in drupal                  |

E.g. usage

```js
import { denormalize, MenuInterface, NormalizedMenuInterface } from '@drupal/decoupled-menu-parser';

mainMenu = a_custom_method_that_fetch_data_from_drupal();
// Where mainMenu is the data returned from Drupal.
const menus = denormalize(JSON.parse(mainMenu);
```

## Development

Development of this project is currently happening in [Drupal.org](https://www.drupal.org/project/decoupled_menu_parser)

## Maintainers

Current maintainers:

- Gabe Sullice ([gabesullice](https://www.drupal.org/u/gabesullice))
- Brian Perry([brianperry](https://www.drupal.org/u/brianperry))
